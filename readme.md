# Import and directly include a remote cmake project

Several ways exist to deal with external git repositories. A common approach is
the use of the git submodules. This approach can quickly become a burden when
building as this dependency may not always be required (optionality), especially
in CI pipelines. This cmake script aims to easily integrate external git
dependencies in CMake builds.


## Examples

Importing a git repository example/example from gitlab.com:

```cmake
import_remote_project("example/example" GITLAB)
```

Importing a git repository example/example with a custom remote project id
(to fetch metadata or to allow recursive inclusion) [recommended]:

```cmake
import_remote_project("example/example" GITLAB ALIAS ext_example)
```


Importing a git repository example/example from github.com:

```cmake
import_remote_project("example/example" GITHUB)
```

Importing a git repository example/example from bitbucket.com:

```cmake
import_remote_project("example/example" BITBUCKET)
```

Importing a git repository example/example from an arbitrary host:

```cmake
import_remote_project("example/example" HOST "https://mygithost.local")
```

Importing a with a custom commit/branch

```cmake
import_remote_project("example/example" COMMIT "tag-1.2.3")
```

Importing a to force using a local directory instead of the remote git repository

```cmake
import_remote_project("example/example" COMMIT "/some/absolute/path/")
```

Importing a from a custom git repository

```cmake
import_remote_project("foo/bar" HOST "mygitlab.local")
```



## Variables

* `<remote_project_id>_path`: Path where the remote git repository has been cloned
