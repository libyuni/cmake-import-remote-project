# \\! Import a cmake from a remote git repository
#
# Example, git repository example/example from gitlab.com:
#   import_remote_project("example/example" GITLAB)
#
# Example, git repository example/example from github.com:
#   import_remote_project("example/example" GITHUB)
#
# Example, git repository example/example from bitbucket.com:
#   import_remote_project("example/example" BITBUCKET)
#
# Example, git repository example/example from an arbitrary host:
#   import_remote_project("example/example" HOST "https://mygithost.local")
#
# Example, with a custom commit/branch
#   import_remote_project("example/example" COMMIT "tag-1.2.3")
#
# Example, to force using a local directory instead of the remote git repository
#   import_remote_project("example/example" COMMIT "/some/absolute/path/")
#
# Example, from a custom git repository
#   import_remote_project("foo/bar" HOST "mygitlab.local")
#
if (COMMAND "import_remote_project")
	return()
endif()

set(import_remote_project_git_bin "git"
	CACHE STRING "Git binary")
set(import_remote_project_path "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/_deps"
	CACHE STRING "Folder for importing projects")
set(import_remote_project_bin_path "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/_deps.build"
	CACHE STRING "Binary dir for building projects")
set(import_remote_project_default_host "https://gitlab.com"
	CACHE STRING "Default git host")
set(import_remote_project_default_commit "master"
	CACHE STRING "Default git branch for remote projects")
set(import_remote_project_default_source "./"
	CACHE STRING "Default subdirectory where to find the main CMakeLists.txt")

find_package(Git REQUIRED) # GIT_EXECUTABLE

macro(irp_impl_print text)
	message(STATUS "{remote-project} ${text}")
endmacro()

irp_impl_print("default host: ${import_remote_project_default_host}<project-id>")
irp_impl_print("git: ${GIT_EXECUTABLE}")

# reset at each cmake re-run to consider branch updates
set(irp_impl_data_keys "" CACHE INTERNAL "" FORCE)

function(irp_impl_registry_find key varname)
	string(REGEX MATCH ";${key}#([^;#]+);" match "${irp_impl_data_keys}")
	if ("${CMAKE_MATCH_1}" STREQUAL "")
		set("${varname}" PARENT_SCOPE)
	else()
		set("${varname}" "${CMAKE_MATCH_1}" PARENT_SCOPE)
	endif()
endfunction()

function(irp_impl_registry_add key branch)
	set(irp_impl_data_keys "${irp_impl_data_keys};${key}#${branch};" CACHE INTERNAL "" FORCE)
endfunction()

function(irp_impl_git_init destdir)
	if (EXISTS "${destdir}")
		message(FATAL_ERROR "The folder '${destdir}' already exists. Some manual cleanup may be required.")
	endif()
	execute_process(
		COMMAND "${GIT_EXECUTABLE}" init "${destdir}"
		RESULT_VARIABLE exitstatus ERROR_VARIABLE err
		OUTPUT_QUIET
	)
	if (NOT "${exitstatus}" EQUAL 0)
		message(FATAL_ERROR "failed to initialize empty git repository '${destdir}'\n${err}")
	endif()
endfunction()

function(irp_impl_git_add_remote destdir host path)
	execute_process(
		COMMAND "${GIT_EXECUTABLE}" remote add "origin" "${host}/${path}"
		WORKING_DIRECTORY "${destdir}"
		RESULT_VARIABLE exitstatus ERROR_VARIABLE err
		OUTPUT_QUIET
	)
	if (NOT "${exitstatus}" EQUAL 0)
		message(FATAL_ERROR "failed to add remote to '${host}/${path}'\n${err}")
	endif()
endfunction()

function(irp_impl_git_fetch destdir)
	execute_process(
		COMMAND "${GIT_EXECUTABLE}" fetch --prune "origin"
		WORKING_DIRECTORY "${destdir}"
		RESULT_VARIABLE exitstatus ERROR_VARIABLE err
		OUTPUT_QUIET
	)
	if (NOT "${exitstatus}" EQUAL 0)
		message(FATAL_ERROR "failed to fetch 'origin' from '${destdir}'\n${err}")
	endif()
endfunction()

function(irp_impl_git_checkout destdir branch)
	execute_process(
		COMMAND "${GIT_EXECUTABLE}" checkout "${branch}"
		WORKING_DIRECTORY "${destdir}"
		RESULT_VARIABLE exitstatus ERROR_VARIABLE err
		OUTPUT_QUIET
	)
	if (NOT "${exitstatus}" EQUAL 0)
		message(FATAL_ERROR "failed to checkout branch or commit id '${branch}' from '${destdir}'\n${err}")
	endif()
endfunction()

function(irp_impl_git_do_all)
	set(options)
	set(oneValueArgs BRANCH HOST PATH PROJECT_ID VAR KEY)
	set(multiValueArgs)
	cmake_parse_arguments(OPTS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
	set(branch "${OPTS_BRANCH}")
	set(host "${OPTS_HOST}")
	set(var "${OPTS_VAR}")
	set(project_id "${OPTS_PROJECT_ID}")
	set(key "${OPTS_KEY}")
	string(SUBSTRING "${branch}" 0 1 first_char)
	if (NOT "${first_char}" STREQUAL "/")
		set(destdir "${import_remote_project_path}/${key}")
		set("${var}" "${destdir}" PARENT_SCOPE)
		if (NOT EXISTS "${destdir}/.git")
			irp_impl_print("initializing ${project_id}")
			irp_impl_git_init("${destdir}")
			irp_impl_git_add_remote("${destdir}" "${host}" "${OPTS_PATH}")
		endif()
		irp_impl_print("updating ${project_id} [origin/${branch}]")
		irp_impl_git_fetch("${destdir}")
		irp_impl_git_checkout("${destdir}" "${branch}")
	else()
		irp_impl_print("using ${project_id} from local path '${branch}'")
		set("${var}" "${branch}" PARENT_SCOPE)
		if (NOT EXISTS "${branch}")
			message(FATAL_ERROR "local folder '${branch}' for '${project_id}' is not accessible")
		endif()
	endif()
endfunction()

function (irp_impl_set_var parent_varname value defvalue)
	if ("${value}" STREQUAL "")
		set(value "${defvalue}")
	endif()
	if (NOT "${value}" MATCHES "[a-zA-Z0-9_-]+[.a-zA-Z0-9_ /-]+")
		message(FATAL_ERROR "invalid identifier for ${parent_varname}")
	endif()
	set("${parent_varname}" "${value}" PARENT_SCOPE)
endfunction()

function (irp_impl_make_key parent_varname host path)
	set(key "${host}--${path}")
	string(REPLACE "/" "--" key "${key}")
	string(REPLACE ":" "-" key "${key}")
	string(REPLACE "--" "-" key "${key}")
	string(REPLACE "--" "-" key "${key}")
	string(REPLACE "--" "-" key "${key}")
	set("${parent_varname}" "${key}" PARENT_SCOPE)
endfunction()

function (import_remote_project input)
	set(options GITLAB GITHUB BITBUCKET NO_CMAKE)
	set(oneValueArgs HOST COMMIT ALIAS SRC)
	set(multiValueArgs)
	cmake_parse_arguments(OPTS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
	set(default_host "${import_remote_project_default_host}")
	if ("${OPTS_GITHUB}")
		set(default_host "https://github.com")
	endif()
	if ("${OPTS_GITLAB}")
		set(default_host "https://gitlab.com")
	endif()
	if ("${OPTS_BITBUCKET}")
		set(default_host "https://bitbucket.com")
	endif()
	irp_impl_set_var(remote_path "${input}" "")
	irp_impl_set_var(remote_commit "${OPTS_COMMIT}" "${import_remote_project_default_commit}")
	irp_impl_set_var(remote_host "${OPTS_HOST}" "${default_host}")
	irp_impl_set_var(remote_alias "${OPTS_ALIAS}" "${remote_path}")
	irp_impl_make_key(remote_key "${remote_host}" "${remote_path}")
	irp_impl_registry_find("${remote_key}" "previous_branch")
	if ("${previous_branch}" STREQUAL "")
		# not cloned yet
		irp_impl_registry_add("${remote_key}" "${remote_commit}")
		irp_impl_git_do_all(
			HOST "${remote_host}" PATH "${remote_path}" BRANCH "${remote_commit}"
			KEY "${remote_key}" PROJECT_ID "${remote_host}/${remote_path}"
			VAR dest
		)
		set("${remote_alias}_path" "${dest}" CACHE STRING "" FORCE)
		set(src "${OPTS_SRC}")
		if ("${src}" STREQUAL "")
			set(src "${import_remote_project_default_source}")
		endif()
		if (NOT "${OPTS_NO_CMAKE}")
			add_subdirectory("${dest}/${src}"
				"${import_remote_project_bin_path}/${remote_key}" EXCLUDE_FROM_ALL)
		endif()
	else()
		if ("${previous_branch}" STREQUAL "${remote_commit}")
			irp_impl_print("${remote_path}:${remote_commit} already added. Ignoring.")
		else()
			irp_impl_print("${remote_path}:${remote_commit} already added but with a different branch (from '${previous_branch}'). Ignoring.")
		endif()

	endif()
endfunction()
